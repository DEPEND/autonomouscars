#! /usr/bin/env python

# Copyright (c) 2020 DEPEND Research Group at
# University of Illinois, Urbana Champaign (UIUC)
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.

from ReportParsers import *

DATASETS = [
   NissanRawDataParser(2016, "raw_csv_files/nissan_2016_totalmiles.csv", "raw_csv_files/nissan_2016_disengagements.csv"),
   NissanRawDataParser(2015, "raw_csv_files/nissan_2015_totalmiles.csv", "raw_csv_files/nissan_2015_disengagements.csv"),
   TeslaRawDataParser(2016, 'raw_csv_files/tesla_2016_totalmiles.csv', 'raw_csv_files/tesla_2016_disengagements.csv'),
   GMCruiseRawDataParser(2016, 'raw_csv_files/gmcruise_2016_totalmiles.csv', 'raw_csv_files/gmcruise_2016_disengagements.csv'),
   GMCruiseRawDataParser(2015, 'raw_csv_files/gmcruise_2015_totalmiles.csv', 'raw_csv_files/gmcruise_2015_disengagements.csv'),
   BoschRawDataParser(2016, 'raw_csv_files/bosch_2016_totalmiles.csv'),
   BoschRawDataParser(2015, 'raw_csv_files/bosch_2015_totalmiles.csv'),
   DelphiRawDataParser(2016, 'raw_csv_files/delphi_2016_totalmiles.csv', 'raw_csv_files/delphi_2016_disengagements.csv'),
   DelphiRawDataParser(2015, 'raw_csv_files/delphi_2015_totalmiles.csv', 'raw_csv_files/delphi_2015_disengagements.csv'),
   BenzRawDataParser(2016, 'raw_csv_files/benz_2016_disengagements.csv',None),
   BenzRawDataParser(2015, 'raw_csv_files/benz_2015_disengagements.csv','raw_csv_files/benz_2015_totalmiles.csv'),
   VolkswagenRawDataParser(2015, 'raw_csv_files/volkswagen_2015_totalmiles.csv','raw_csv_files/volkswagen_2015_disengagements.csv'),
   WaymoRawDataParser(2016, 'raw_csv_files/waymo_2016_totalmiles.csv', 'raw_csv_files/waymo_2016_disengagements.csv'),
   WaymoRawDataParser(2015, 'raw_csv_files/waymo_2015_totalmiles.csv', 'raw_csv_files/waymo_2015_disengagements.csv')
]

def createTBFPlotCSVFiles(TBF):
    """Output the CSV files corresponding to TBF plots"""
    pass

def createFPMPlotCSVFiles(FPM):
    """Output the CSV files corresponding to the FPM plots"""
    pass

if __name__ == '__main__':
    # Generate TBF dataset
    TBF = [d.parseTBF() for d in DATASETS if len(d.parseTBF()) > 0]
    FPM = [d.parseFPM() for d in DATASETS if len(d.parseFPM()) > 0]
    RC = [d.parseRoadTypes() for d in DATASETS]
    RT = [d.parseReactionTimes() for d in DATASETS]
    AM = [d.parseManualAutomatic() for d in DATASETS]
    WFC = [d.parseWeatherWithFails() for d in DATASETS]

    print("TBF", TBF)
    print("\n\nFPM", FPM)
    print("\n\nRC", RC)
    print("\n\nRT", RT)
    print("\n\nAM", AM)
    print("\n\nWeather:",WFC)
