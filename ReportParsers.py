#! /usr/bin/env python

# Copyright (c) 2020 DEPEND Research Group at
# University of Illinois, Urbana Champaign (UIUC)
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.

import pandas as pd
import datetime as dt

import argparse
import sys
import os
from pprint import pprint
import csv
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
stopset = set(stopwords.words('english'))

confFile = {
    "nissan": "CategoryDefinition/nissan.tsv",
    "google": "CategoryDefinition/google.tsv",
    "volkswagon": "CategoryDefinition/volkswagon.tsv",
    "delphi": "CategoryDefinition/delphi.tsv",
    "tesla": "CategoryDefinition/tesla.tsv"
}

class RawDataParser(object):
    """Abstract class for CSV data parser"""
    def parseTBF(self):
        """Returns a dict of the form { "car1": [TBF1, TBF2, TBF3...] } (TBFs are specified in hours)"""
        pass

    def parseFPM(self):
        """Returns a dict of the form { "car1": { "month-year": {"miles":m , "failures": f}...}, ... }"""
        pass

    def parseReactionTimes(self):
        """Returns a dict of the form {"month-year":[t1,t2,t3...]} where t in seconds"""
        pass

    def parseRoadTypes(self):
        """Returns a dict of the form { "road_type1": num_disengamenets, ... }"""
        pass

    def parseManualAutomatic(self):
        """Return a dict of the form { "manual": frac, "auto": frac, "planned": frac }"""
        pass

    def __parser_dict__(self, fname):
        parserDict = {}
        lineNum = 0
        for line in open(fname, "rU"):
            line = line.strip('\"')
            line = line.strip().lower()
            cols = line.split("\t")
            if len(cols) < 3:
                print("invalid conf file, exiting")
                print(cols)
                exit(1)
            tokens = word_tokenize(cols[0])
            tokens = [w.lower() for w in tokens if not w in stopset]
            parserDict[lineNum] = [tokens, cols[1], cols[2]]
            lineNum += 1
        parserDict[0] = [["Unknown"], "Unknown", "Unknown"]
        return parserDict

    def __maximal_matching__(self, line, parserDict):
        #maintain score for id
        score = {}
        line = line.strip()
        line = line.strip("\"")
        tokens=word_tokenize(line)
        tokens = [w.lower() for w in tokens if not w in stopset]
        line_word_set = set(tokens)
        max_score_id = 0
        max_score_val = 0
        #print("lineset", line_word_set)
        for k in parserDict:
            #print("category words", parserDict[k][0][0])
            cat_word_set = set(parserDict[k][0])
            score[k] = len(cat_word_set.intersection(line_word_set))
        #    print(cat_word_set, "\n", cat_word_set.intersection(line_word_set))
            #print("\nscore for this id", score[k])
            if score[k] > max_score_val:
                max_score_val = score[k]
                max_score_id = k
            #print("max-score", "max-id", max_score_val, max_score_id)
        return max_score_id

    def __ftag__(self, args):
        line = " ".join([str(ele) for ele in args])
        if self.categoryDef == None:
            return ""
        return self.categoryDef[self.__maximal_matching__(line, self.categoryDef)][1]

    def __fcat__(self, args):
        line = " ".join([str(ele) for ele in args])
        if self.categoryDef == None:
            return ""
        return self.categoryDef[self.__maximal_matching__(line, self.categoryDef)][2]

    def __category_annotator__(self):
        self.raw_disengagements['ftag'] = self.raw_disengagements.apply(self.__ftag__, axis=1)
        self.raw_disengagements['fcat'] = self.raw_disengagements.apply(self.__fcat__, axis=1)
        return

    def parseCategories(self):
        """Pandas dataframe with level1 and level2 categories"""
        self.__category_annotator__()
        return self.raw_disengagements[['ftag', 'fcat']]

    def parseWeatherWithFails(self):
        """returns a dict of the form {"overall":DPM,"condition1":DPM1...}"""
        pass

class TeslaRawDataParser(RawDataParser):
    """Parse data from Tesla files"""
    def __init__(self, year, fname_miles, fname_disengagements):
        self.manufacturer = "Tesla"
        self.year = year
        self.raw_miles = pd.read_csv(fname_miles, sep=',')
        self.raw_disengagements = pd.read_csv(fname_disengagements, sep=',')
        self.categoryDef = self.__parser_dict__(confFile["tesla"])
        self.__category_annotator__()

    def parseTBF(self):
        # Cannot compute TBF from data
        return {}

    def parseFPM(self):
        fpm = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%m/%d/%Y').strftime('%b-%Y')
        for c in self.raw_miles.columns.values[1:].tolist():
            # Get number of failures
            failures = dict(
                    self.raw_disengagements[(self.raw_disengagements.VIN == c)]['Date'].apply(datetomonth).value_counts()
                )
            # Get miles driven and create return object
            fpm[c] = {
                '%s-%d'%(dat['Month'], self.year): {
                    'miles': dat[c],
                    'failures': failures.get('%s-%d'%(dat['Month'], self.year), 0)
                }
                for idx, dat in self.raw_miles.loc[:, ['Month', c]].iterrows()
                if dat[c] > 0
            }
        return fpm

    def parseReactionTimes(self):
        rt_permonth = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%m/%d/%Y').strftime('%b-%Y')
        self.raw_disengagements["Month"] = self.raw_disengagements['Date'].apply(datetomonth);
        for month in self.raw_disengagements['Month'].unique():
            rt = [
                dat['Time to take over(s)']
                for idx,dat in self.raw_disengagements[self.raw_disengagements['Month'] == month].iterrows()
            ]
            rt_permonth[month] = rt

        return rt_permonth

    def parseRoadTypes(self):
        return self.raw_disengagements["Road Class"].value_counts().to_dict()

    def parseManualAutomatic(self):
        self.raw_disengagements['Dtype'] = self.raw_disengagements['Cause for Disangement'].apply(lambda x: "A" if x.startswith('ACC Cancel') else "M")
        num_manual = self.raw_disengagements[self.raw_disengagements['Cause for Disangement'] == 'ACC Cancel'].shape[0]
        num_auto = self.raw_disengagements.shape[0] - num_manual
        return {
            "manual": num_manual / (num_manual + num_auto),
            "auto": num_auto / (num_manual + num_auto),
            "planned": 0
        }


class NissanRawDataParser(RawDataParser):
    """Parse data from Nissan files"""
    def __init__(self, year, fname_miles, fname_disengagements):
        self.manufacturer = 'Nissan'
        self.year = year
        self.raw_miles = pd.read_csv(fname_miles, sep=',')
        self.raw_disengagements = pd.read_csv(fname_disengagements, sep=',')
        self.categoryDef = self.__parser_dict__(confFile["nissan"])
        self.__category_annotator__()

    def parseTBF(self):
        tbf = {}
        for c in self.raw_disengagements['ID'].unique():
            ft = [
                dt.datetime.strptime("%s %s"%(dat['Date'], dat['Time']), '%m/%d/%Y %I:%M %p')
                for idx, dat in self.raw_disengagements[self.raw_disengagements['ID'] == c].iterrows()
            ]
            tbf[c] = [(x - ft[i - 1]).total_seconds()/3600 for i, x in enumerate(ft)][1:]
        return tbf

    def parseFPM(self):
        return {
            "total" : {
                '%s-%s'%(dt.datetime.strptime(dat['Month'],'%B').strftime('%b'), dat['Year']): {
                    'miles': dat['Miles'],
                    'failures': dat['Disengagements']
                }
                for idx, dat in self.raw_miles.iterrows()
            }
        }

    def parseReactionTimes(self):
        rt_permonth = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%m/%d/%Y').strftime('%b-%Y')
        self.raw_disengagements["Month"] = self.raw_disengagements['Date'].apply(datetomonth);
        for month in self.raw_disengagements['Month'].unique():
            rt = [
                dat['ElapsedTime']
                for idx,dat in self.raw_disengagements[self.raw_disengagements['Month'] == month].iterrows()
            ]
            rt_permonth[month] = rt
        return rt_permonth

    def parseRoadTypes(self):
        return self.raw_disengagements["Locations"].value_counts().to_dict()

    def parseManualAutomatic(self):
        #Add Row for DisengType
        self.raw_disengagements['Dtype'] = self.raw_disengagements['Type'].apply(lambda x: "A" if x.startswith('1') else "M")
        num_auto = self.raw_disengagements[self.raw_disengagements['Type'].str.startswith('1')].shape[0]
        num_manual = self.raw_disengagements.shape[0] - num_auto
        return {
            "manual": num_manual / (num_manual + num_auto),
            "auto": num_auto / (num_manual + num_auto),
            "planned": 0
        }

    def parseWeatherWithFails(self):
        
        sunny=0
        cloudy=0
        rainy=0
        night=0
        dry=0
        wet=0
        total_failures=0

        for idx,dat in self.raw_disengagements.iterrows():
            total_failures+=1
            condition_list=dat["Weather"].split('/')
            if condition_list[0]=="Cloudy":
                cloudy+=1
            elif condition_list[0]=="Sunny":
                sunny+=1
            elif condition_list[0]=="Clear(night)":
                night+=1
            elif condition_list[0]=="Rainy":
                rainy+=1

            if condition_list[1]=="Dry":
                dry+=1
            elif condition_list[1]=="Wet":
                wet+=1
        return {
            "overall": total_failures,
            "sunny": sunny,
            "cloudy": cloudy,
            "rainy": rainy,
            "night": night,
            "dry": dry,
            "wet": wet
        }

class GMCruiseRawDataParser(RawDataParser):
    """Parse data from GMCruise files"""
    def __init__(self, year, fname_miles, fname_disengagements):
        self.manufacturer = 'GMCruise'
        self.year = year
        self.raw_miles = pd.read_csv(fname_miles, sep=',')
        self.raw_disengagements = pd.read_csv(fname_disengagements, sep = ',')
        self.categoryDef = None
        self.__category_annotator__()

    def parseTBF(self):
        """GM does not attach vehicle names to the failure instances"""
        """This would mean there will be only 1 list of TBFs"""
        tbf = {}
        ft = [
                dt.datetime.strptime("%s %s"%(dat['Date'],dat['Time']), "%m/%d/%Y %H:%M")
                for idx, dat in self.raw_disengagements.iterrows()
        ]
        tbf[0] = [(x - ft[i-1]).total_seconds()/3600 for i, x in enumerate(ft)][1:]
        return tbf

    def parseFPM(self):
        """GM does not provide per car failures"""
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%m/%d/%Y').strftime('%b-%Y')
        # Get Total Failure count per month
        failures = dict(
            self.raw_disengagements['Date'].apply(datetomonth).value_counts()
        )
        return {
            "total" : {
                '%s-%d'%(dt.datetime.strptime(dat['Month and year'][:3],'%b').strftime('%b'), self.year): {
                'miles': dat['Autonomous miles'],
                'failures': failures.get("%s-%d"%(dat['Month and year'][:3],self.year),0)
                }
                for idx,dat in self.raw_miles.iterrows()
            }
        }

    def parseReactionTimes(self):
        """Cannot parse reaction times as Data not available"""
        return {}

    def parseRoadTypes(self):
        """GM does not provide road types"""
        return {}

    def parseManualAutomatic(self):
        self.raw_disengagements['Dtype'] = "P"
        return {
            "manual": 0,
            "auto": 0,
            "planned": 1
        }


class BoschRawDataParser(RawDataParser):
    """Parse data from Bosch files"""
    """Bosch has only 1 file to be processed - totalmiles"""
    def __init__(self, year, fname_miles):
        self.manufacturer = 'Bosch'
        self.year = year
        self.raw_disengagements = pd.DataFrame()
        self.raw_miles = pd.read_csv(fname_miles, sep=',')
        self.categoryDef = None
        self.__category_annotator__()

    def parseTBF(self):
        """Not possible to calculate TBFs with given dataset"""
        return {}

    def parseFPM(self):
        """NOTE: Bosch has miles driven per car per month but only total disengagements across all cars per month"""
        """Hence, FPM will not be calculated on a per car basis due to lack of data"""
        #Extract total miles per month
        col_list = ['car-049M272 miles', 'car-007M285 miles', 'car-049M273 miles']

        return{
            "total" : {
                "%s"%(dt.datetime.strptime(dat['Month'],'%y-%b').strftime('%b-%Y')):{
                    'miles': round(dat[col_list].sum(),1),
                    'failures': dat['Total Disengagements']
                } for idx,dat in self.raw_miles.iterrows()
                 if round(dat[col_list].sum(),1) >0
            }
        }

    def parseReactionTimes(self):
        """Cannot parse reaction times as Data not available"""
        return {}

    def parseRoadTypes(self):
        raw_data = self.raw_miles['Road Types'].apply(lambda x: dict(zip(['Interstate', 'Freeway', 'Highway', 'Rural Roads', 'Parking Lots'], map(lambda y: int(y), x.split('/'))))).tolist()
        return pd.DataFrame(raw_data).sum().to_dict()

    def parseManualAutomatic(self):

        self.raw_disengagements['Dtype'] = "P"
        return {
            "manual": 0,
            "auto": 0,
            "planned": 1
        }


class DelphiRawDataParser(RawDataParser):
    """Parse data from Delphi files"""
    def __init__ (self,year,fname_miles,fname_disengagements):
        self.manufacturer = 'Delphi'
        self.year = year
        self.raw_miles = pd.read_csv(fname_miles, sep=',')
        self.raw_disengagements = pd.read_csv(fname_disengagements, sep=',')
        self.categoryDef = self.__parser_dict__(confFile["delphi"])
        self.__category_annotator__()

    def parseTBF(self):
        """Not possible to calculate TBFs with given dataset"""
        return {}

    def parseFPM(self):
        #get number of failures
        fpm={}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%y-%b').strftime('%b-%Y')
        for c in self.raw_disengagements['Car'].unique():
            failures = dict(
                self.raw_disengagements.loc[self.raw_disengagements['Car']==c]['Month'].apply(datetomonth).value_counts()
            )

            fpm[c] = {
                '%s'%(datetomonth(dat['Month'])): {
                    'miles': dat[c],
                    'failures': failures.get('%s'%(datetomonth(dat['Month'])), 0)
                }
                for idx,dat in self.raw_miles.iterrows()
                if dat[c] > 0
            }
        return fpm

    def parseReactionTimes(self):
        rt_permonth = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%y-%b').strftime('%b-%Y')
        self.raw_disengagements["Month_Formatted"] = self.raw_disengagements['Month'].apply(datetomonth);
        for month in self.raw_disengagements['Month_Formatted'].unique():
            rt = [
                dat['Time to Disengagement']
                for idx,dat in self.raw_disengagements[self.raw_disengagements['Month_Formatted'] == month].iterrows()
            ]
            rt_permonth[month] = rt
        return rt_permonth

    def parseRoadTypes(self):
        return self.raw_disengagements["Location"].value_counts().to_dict()

    def parseManualAutomatic(self):
        """Cannot make out cause from text"""
        self.raw_disengagements['Dtype'] = "NA"
        return {}


class BenzRawDataParser(RawDataParser):
    """Parse data from the Benz files"""
    def __init__ (self, year, fname_disengagements, fname_miles):
        self.manufacturer = 'Benz'
        self.year = year
        self.raw_disengagements = pd.read_csv(fname_disengagements)
        if(fname_miles != None):
            self.raw_miles = pd.read_csv(fname_miles)
        self.categoryDef = None
        self.__category_annotator__()

    def parseTBF(self):
        """Not possible to calculate TBFs with 2016  dataset"""
        return {}

    def parseFPM(self):
        if(self.year == 2016):
            """'Per car data not available"""
            datetomonth = lambda x: dt.datetime.strptime(str(x), '%b-%y').strftime('%b-%Y')
            col_list = ['Auto Disengagement','Manual Disengagement']
            return {
                'totalBenz':{
                    '%s'%(datetomonth(dat['Month'])) :{
                        'miles': float(dat['Miles']),
                        'failures': float(round(dat[col_list].sum(),0))
                    }
                    for idx,dat in self.raw_disengagements.iterrows()
                }
            }
        """ For 2015 dataset """
        fpm = {}
        for c in self.raw_miles['Car'].unique():
            # Get miles driven and create return object
            fpm[c] = {
                '%s'%(dat['Month']): {
                    'miles': dat['miles driven in autonomous mode'],
                    'failures': '%s'%(dat['total number of disengagements'])
                }
                for idx, dat in self.raw_miles.loc[(self.raw_miles['Car'] == c)].iterrows()
                if dat['miles driven in autonomous mode'] > 0
            }
        return fpm

    def parseReactionTimes(self):
        """Cannot parse reaction times as Data not available for 2016"""
        if(self.year == 2016):
            return {}

        rt_permonth = {}
        for month in self.raw_disengagements['Month'].unique():
            rt = [
                dat['ReactionTime']
                for idx,dat in self.raw_disengagements[self.raw_disengagements['Month'] == month].iterrows()
            ]
            rt_permonth[month] = rt
        return rt_permonth

    def parseRoadTypes(self):
        """Benz does not provide road types"""
        if  "Location" in self.raw_disengagements.columns:
            return self.raw_disengagements["Location"].value_counts().to_dict()
        else:
            return {}

    def parseManualAutomatic(self):
        if(self.year == 2016):
            self.raw_disengagements['Dtype'] = "NA"
            num_auto = self.raw_disengagements["Auto Disengagement"].sum()
            num_manual = self.raw_disengagements["Manual Disengagement"].sum()
            return {
                "manual": num_manual / (num_manual + num_auto),
                "auto": num_auto / (num_manual + num_auto),
                "planned": 0
            }
        else:
            self.raw_disengagements['Dtype'] = self.raw_disengagements['TypeOfTrigger'].apply(lambda x: "A" if x.startswith('a') else "M")
            num_auto = self.raw_miles["number of automatic disengagements"].sum()
            num_manual = self.raw_miles["number of manual disengagements"].sum()
            return {
                "manual": num_manual / (num_manual + num_auto),
                "auto": num_auto / (num_manual + num_auto),
                "planned": 0
            }



class VolkswagenRawDataParser(RawDataParser):
    def __init__(self,year,fname_miles,fname_disengagements):
        self.manufacturer = 'Volkswagen'
        self.year = year
        self.raw_disengagements = pd.read_csv(fname_disengagements)
        self.raw_miles = pd.read_csv(fname_miles)
        self.categoryDef = self.__parser_dict__(confFile["volkswagon"])
        self.__category_annotator__()

    def parseTBF(self):
        tbf = {}
        for c in self.raw_disengagements['Car'].unique():
            ft = [
                dt.datetime.strptime("%s %s"%(dat['Date'], dat['Time']), '%m/%d/%Y %H:%M:%S')
                for idx, dat in self.raw_disengagements[self.raw_disengagements['Car'] == c].iterrows()
            ]
            tbf[c] = [(x - ft[i - 1]).total_seconds()/3600 for i, x in enumerate(ft)][1:]
        return tbf

    def parseFPM(self):
        fpm = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%m/%d/%Y').strftime('%b-%Y')
        datetomonth2 = lambda x: dt.datetime.strptime(str(x), '%y-%b').strftime('%b-%Y')
        for c in self.raw_miles.columns.values[1:].tolist():
            failures = dict(
                        self.raw_disengagements[(self.raw_disengagements.Car == c)]['Date'].apply(datetomonth).value_counts()
            )

            fpm[c] = {
                '%s'%(datetomonth2(dat['Month'])):{
                    'miles': dat[c],
                    'failures': failures.get(datetomonth2(dat['Month']), 0)
                }
                for idx,dat in self.raw_miles.loc[:,['Month', c]].iterrows()
                if dat[c] > 0
            }

        return fpm

    def parseReactionTimes(self):
        rt_permonth = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%m/%d/%Y').strftime('%b-%Y')
        self.raw_disengagements["Month_Formatted"] = self.raw_disengagements['Date'].apply(datetomonth);
        for month in self.raw_disengagements['Month_Formatted'].unique():
            rt = [
                dat['ReactionTime']
                for idx,dat in self.raw_disengagements[self.raw_disengagements['Month_Formatted'] == month].iterrows()
            ]
            rt_permonth[month] = rt
        return rt_permonth

    def parseRoadTypes(self):
        """Volkswagen does not provide road types"""
        return {}

    def parseManualAutomatic(self):
        self.raw_disengagements['Dtype'] = self.raw_disengagements['StateMachineMode'].apply(lambda x: "A" if x.startswith('Takeover_R') else "M")
        num_auto = self.raw_disengagements[self.raw_disengagements["StateMachineMode"] == "Takeover-Request"].shape[0]
        num_manual = self.raw_disengagements.shape[0] - num_auto
        return {
            "manual": num_manual / (num_manual + num_auto),
            "auto": num_auto / (num_manual + num_auto),
            "planned": 0
        }


class WaymoRawDataParser(RawDataParser):
    def __init__(self,year,fname_miles,fname_disengagements):
        self.manufacturer = 'Waymo'
        self.year = year
        self.raw_disengagements = pd.read_csv(fname_disengagements)
        self.raw_miles = pd.read_csv(fname_miles)
        self.categoryDef = self.__parser_dict__(confFile["google"])
        self.__category_annotator__()

    def parseTBF(self):
        """TBF cannot be calculated for Waymo due to insufficent data available"""
        return {}

    def parseFPM(self):
        return {
            'total':{
                "%s"%(dt.datetime.strptime(dat['Month'],'%m/%y').strftime('%b-%Y')):{
                    'miles': dat['AutonomousMiles'],
                    'failures': dat['Disengagements']
                }for idx,dat in self.raw_miles.iterrows()
            }
        }

    def parseReactionTimes(self):
        rt_permonth = {}
        datetomonth = lambda x: dt.datetime.strptime(str(x), '%b %Y').strftime('%b-%Y')
        self.raw_disengagements["Month_Formatted"] = self.raw_disengagements['Date'].apply(datetomonth);
        for month in self.raw_disengagements['Month_Formatted'].unique():
            rt = [
                dat['TimeToManual']
                for idx,dat in self.raw_disengagements[self.raw_disengagements['Month_Formatted'] == month].iterrows()
            ]
            rt_permonth[month] = rt
        return rt_permonth

    def parseRoadTypes(self):
        return self.raw_disengagements["Location"].value_counts().to_dict()

    def parseManualAutomatic(self):
        self.raw_disengagements['Dtype'] = self.raw_disengagements['Type'].apply(lambda x: "A" if x.startswith('Failure Detection') else "M")
        num_auto = self.raw_disengagements[self.raw_disengagements["Type"] == "Failure Detection"].shape[0]
        num_manual = self.raw_disengagements.shape[0] - num_auto
        return {
            "manual": num_manual / (num_manual + num_auto),
            "auto": num_auto / (num_manual + num_auto),
            "planned": 0
        }
