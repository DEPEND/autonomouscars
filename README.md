# Analyzing Failures in Autonomous Vehicles

## Run Visualization Code
1. Run Jupyter Notebook via Docker
```
docker run -d -v <path to workspace>:/home/jovyan/work -p 8888:8888 jupyter/datascience-notebook start-notebook.sh --NotebookApp.token=''
```
2. Visit http://localhost:8888/

## Dataset Sources
- California DMV - Autonomous Vehicle Testing (https://www.dmv.ca.gov/portal/dmv/detail/vr/autonomous+/testing)
- California DMV - Autonomous Vehicle Accident Reports (https://www.dmv.ca.gov/portal/dmv/detail/vr/autonomous/autonomousveh_ol316+)
- California DMV - Autonomous Vehicle Disengagement Reports 2015 (https://www.dmv.ca.gov/portal/dmv/detail/vr/autonomous/disengagement_report_2015)
- California DMV - Autonomous Vehicle Disengagement Reports 2016 (https://www.dmv.ca.gov/portal/dmv/detail/vr/autonomous/disengagement_report_2016)

## Publication

Hands off the wheel in autonomous vehicles?: A systems perspective on over a million miles of field data. <br>
Subho S. Banerjee, Saurabh Jha, James Cyriac, Zbigniew T. Kalbarczyk, Ravishankar K. Iyer. DSN 2018
[[PDF](https://ssbaner2.cs.illinois.edu/publications/dsn2018/Paper.pdf)] 

```
@inproceedings{banerjee2018hands,
  title={Hands off the wheel in autonomous vehicles?: A systems perspective on over a million miles of field data},
  author={Banerjee, Subho S and Jha, Saurabh and Cyriac, James and Kalbarczyk, Zbigniew T and Iyer, Ravishankar K},
  booktitle={2018 48th Annual IEEE/IFIP International Conference on Dependable Systems and Networks (DSN)},
  pages={586--597},
  year={2018},
  organization={IEEE}
}
```